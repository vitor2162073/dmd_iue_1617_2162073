//
//  Person.swift
//  PersonTabGit_PL2
//
//  Created by Catarina Silva on 29/09/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class Person {
    var firstName:String?
    var lastName:String?
    var nationality:String //= "Portuguese"
    
    init() {
        self.nationality = "Portuguese"
    }
    
    init(firstName:String, lastName:String, nationality:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    func fullName() -> String {
        return firstName! + " " + lastName! + " - " + nationality
    }
    
}






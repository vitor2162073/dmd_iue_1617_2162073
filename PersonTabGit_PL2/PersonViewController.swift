//
//  PersonViewController.swift
//  PersonTabGit_PL2
//
//  Created by Catarina Silva on 29/09/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var nationalityText: UITextField!
    
    var person : Person?
    
    @IBAction func saveAction(_ sender: AnyObject) {
        if let p = person {
            // p exists and is a person
            p.firstName = firstNameText.text
            p.lastName = lastNameText.text
            p.nationality = nationalityText.text!
        } else {
            let newPerson = Person(firstName: firstNameText.text!, lastName: lastNameText.text!, nationality: nationalityText.text!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.people.append(newPerson)
        }
        self.navigationController!.popViewController(animated: true)
        
    }
    
    @IBAction func setButtonPressed(_ sender: UIButton) {
        /*person.firstName = firstNameText.text
        person.lastName = lastNameText.text
        if let nat = nationalityText.text {
            person.nationality = nat
        }
        //person.nationality = nationalityText.text
        
        labelText.text = person.fullName()
     
        //print("nationality: /(person.nationality)")*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstNameText.text = self.person?.firstName
        self.lastNameText.text = self.person?.lastName
        self.nationalityText.text = self.person?.nationality
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

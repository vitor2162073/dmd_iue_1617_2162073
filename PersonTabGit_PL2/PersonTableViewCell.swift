//
//  PersonTableViewCell.swift
//  PersonTabGit_PL2
//
//  Created by Vítor Dias on 07/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNmaeLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    
    var person:Person? {
        didSet{
            self.firstNameLabel.text = person?.firstName
            self.lastNmaeLabel.text = person?.lastName
            self.nationalityLabel.text = person?.nationality
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MapViewController.swift
//  PersonTabGit_PL2
//
//  Created by Vítor Dias on 06/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var map: MKMapView!
    
    var annotation = MKPointAnnotation()
    
    var location:CLLocationCoordinate2D?
    
    @IBAction func didTapMap(_ sender: UITapGestureRecognizer) {
        let tapAnnotation = MKPointAnnotation()
        let tapPoint = sender.location(in: map)
        location = map.convert(tapPoint, toCoordinateFrom: map)
        
        tapAnnotation.coordinate = location!
        tapAnnotation.title = "Tapped location"
        map.addAnnotation(tapAnnotation)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
        
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
            map.delegate = self
            map.showsUserLocation = true
        }
        else {
            // TODO
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Erro: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            print ("latitude: \(c.latitude) longitude \(c.longitude)")
            
            let span = MKCoordinateSpanMake(0.1, 0.1)
            let region = MKCoordinateRegionMake(c, span)
            
            map.setRegion(region, animated: true)
            
            annotation.coordinate = c
            annotation.title = "User's location"
            annotation.subtitle = "Here!"
            
            map.addAnnotation(annotation)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
